(function () {
   
      
       const config = {
            apiKey: "AIzaSyCKf-M0Rl0dXqYKEfEeMQGljbp0xKFe4jo",
            authDomain: "login-7c276.firebaseapp.com",
            databaseURL: "https://login-7c276.firebaseio.com",
            projectId: "login-7c276",
            storageBucket: "login-7c276.appspot.com",
            messagingSenderId: "278371944698"
        };
    firebase.initializeApp(config);
   
    const txtEmail = document.getElementById('email_signup');
    const txtPassword = document.getElementById('password_signup');
    const btnLogin = document.getElementById('btnLogin');
    const btnSignUp = document.getElementById('btnSignUp')
    const btnLogout = document.getElementById('btnLogout');
    const btnLoginGoogle = document.getElementById('btnLoginGoogle');

    btnLogin.addEventListener('click', e => {
        console.log(txtEmail.value);
        const email= txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();


        console.log(String(email)+ " " + String(pass));
        const promise = auth.signInWithEmailAndPassword(String(email),String(pass));
        promise.catch(e => console.log(e.message));

    });

    btnLoginGoogle.addEventListener('click', e => {
        document.getElementById('btnLogin').style.visibility = 'hidden';
        document.getElementById('btnLogout').style.visibility = 'visible';
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        provider.addScope('https://www.googleapis.com/auth/adexchange.buyer');
        return firebase.auth().signInWithPopup(provider).catch(function (error) {
            console.log('google error Sign In', error);
        });
    });




        btnSignUp.addEventListener('click', e => {
            const email = txtEmail.value;
            const pass = txtPassword.value;
            const auth = firebase.auth();

            const promise = auth.createUserWithEmailAndPassword(email, pass);
            promise.catch(e => console.log(e.message));
        });


        btnLogout.addEventListener('click', e => {
            firebase.auth().signOut();
        });

        //realtime authentication listener
        firebase.auth().onAuthStateChanged(firebaseUser => {
            if (firebaseUser) {
                console.log(firebaseUser);

                btnLogout.classList.remove('hide');
                document.getElementById('btnLogin').style.visibility = 'hidden';
                document.getElementById('btnLogout').style.visibility = 'visible';
            } else {
                console.log("not Logged In!");
                document.getElementById('btnLogout').style.visibility = 'hidden';
                document.getElementById("btnLogin").style.visibility = 'visible';

            }
        });

    }());